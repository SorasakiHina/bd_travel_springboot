package com.neutrino.service.impl;

import com.neutrino.entity.QueryVo;
import com.neutrino.entity.Scenic;
import com.neutrino.entity.TourismInfo;
import com.neutrino.mapper.TourismMapper;
import com.neutrino.service.TourismService;
import com.neutrino.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TourismServiceImpl implements TourismService {

    @Autowired
    TourismMapper tourismMapper;

    public List<TourismInfo> selectTourismInfoList() {
        return tourismMapper.selectTourismInfoList();
    }

    /**
     *后台获得分页数据
     *
     * @param vo
     * @return
     */
    public Page<TourismInfo> selectPageByQueryVo(QueryVo vo) {
        Page<TourismInfo> page = new Page<TourismInfo>();
        //每页数
        page.setSize(5);
        vo.setSize(5);
        if (null != vo) {
            // 判断当前页
            if (null != vo.getPage()) {
                page.setPage(vo.getPage());
                vo.setStartRow((vo.getPage() - 1) * vo.getSize());
            }
            if(null != vo.getName() && !"".equals(vo.getName().trim())){
                vo.setName(vo.getName().trim());
            }
            if(null != vo.getTheme() && !"".equals(vo.getTheme().trim())){
                vo.setTheme(vo.getTheme().trim());
            }
            if(null != vo.getAddr() && !"".equals(vo.getAddr().trim())){
                vo.setAddr(vo.getAddr().trim());
            }
            //总条数
            page.setTotal(tourismMapper.postCountByQueryVo(vo));
            page.setRows(tourismMapper.selectPostListByQueryVo(vo));
        }
        return page;
    }

    @Override
    public TourismInfo selectInfoByName(String name) {
        return tourismMapper.selectInfoByName(name);
    }
}
