package com.neutrino.controller;

import com.neutrino.entity.QueryVo;
import com.neutrino.entity.TourismInfo;
import com.neutrino.service.TourismService;
import com.neutrino.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TourismController {
    @Autowired
    TourismService tourismService;

    @RequestMapping(value = "/getTourismInfo", method = {RequestMethod.GET}, consumes = {"application/x-www-form-urlencoded"})
    @ResponseBody
    public Object getTourismInfo(@RequestParam(name = "tourismName") String tourismName){
        TourismInfo tourismInfo = tourismService.selectInfoByName(tourismName);
        return tourismInfo;
    }

//    @RequestMapping(value = "/getMapByAddr")
//    public String getMapByAddr(Model model, QueryVo vo, HttpServletRequest request){
//        Page<TourismInfo> page = tourismService.selectPageByQueryVo(vo);
//        //根据参数判断目前是不是根据地址获得景点信息
//        String addrParameter = request.getParameter("addr");
//        if (addrParameter!=null){
//            model.addAttribute("addrParameter",addrParameter);
//        }
//        model.addAttribute("page", page);
//        return "user/map";
//    }

}
